"""
Set of nose tests for the acp_times functions.
"""

import nose
import arrow
import acp_times
import logging


class TestOpenTimes:

    def __init__(self):
        # start of race
        self.race_time = arrow.Arrow(2019, 1, 15)

    def test_no_open(self):
        # distances
        brevet_dist = 200
        control_dist = 0
        # expected open time
        shift_calc = control_dist / 34
        expected_open = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"no_open: Expected open time is: {expected_open}")
        calculated_open = acp_times.open_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"no_open: Calculated open time is: {calculated_open}")
        assert calculated_open == expected_open

    def test_edge_open(self):
        # distances
        brevet_dist = 200
        control_dist = 60
        # expected open time
        shift_calc = control_dist / 34
        expected_open = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"edge_open: Expected open time is: {expected_open}")
        calculated_open = acp_times.open_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"edge_open: Calculated open time is: {calculated_open}")
        assert calculated_open == expected_open

    def test_small_open(self):
        # distances
        brevet_dist = 300
        control_dist = 90
        # expected open time
        shift_calc = control_dist / 34
        expected_open = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"small_open: Expected open time is: {expected_open}")
        calculated_open = acp_times.open_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"small_open: Calculated open time is: {calculated_open}")
        assert calculated_open == expected_open

    def test_medium_open(self):
        # distances
        brevet_dist = 600
        control_dist = 450
        # expected open time
        shift_calc = 200 / 34 + 200 / 32 + 50 / 30
        expected_open = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"medium_open: Expected open time is: {expected_open}")
        calculated_open = acp_times.open_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"medium_open: Calculated open time is: {calculated_open}")
        assert calculated_open == expected_open

    def test_big_open(self):
        # distances
        brevet_dist = 1000
        control_dist = 720
        # expected open time
        shift_calc = 200 / 34 + 200 / 32 + 200 / 30 + 120 / 28
        expected_open = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"big_open: Expected open time is: {expected_open}")
        calculated_open = acp_times.open_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"big_open: Calculated open time is: {calculated_open}")
        assert calculated_open == expected_open

    def test_max_open(self):
        # distances
        brevet_dist = 1000
        control_dist = 1000
        # expected open time
        shift_calc = 200 / 34 + 200 / 32 + 200 / 30 + 400 / 28
        expected_open = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"max_open: Expected open time is: {expected_open}")
        calculated_open = acp_times.open_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"max_open: Calculated open time is: {calculated_open}")
        assert calculated_open == expected_open

    def test_over_open(self):
        # distances
        brevet_dist = 600
        control_dist = 615
        # expected open time
        shift_calc = 200 / 34 + 200 / 32 + 200 / 30
        expected_open = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"over_open: Expected open time is: {expected_open}")
        calculated_open = acp_times.open_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"over_open: Calculated open time is: {calculated_open}")
        assert calculated_open == expected_open


class TestCloseTimes:

    def __init__(self):
        # start of race
        self.race_time = arrow.Arrow(2019, 1, 15)

    def test_no_close(self):
        # distances
        brevet_dist = 200
        control_dist = 0
        # expected open time
        shift_calc = 1 + control_dist / 20
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"no_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"no_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close

    def test_edge_close(self):
        # distances
        brevet_dist = 200
        control_dist = 60
        # expected open time
        shift_calc = 1 + control_dist / 20
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"edge_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"edge_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close

    def test_small_close(self):
        # distances
        brevet_dist = 300
        control_dist = 90
        # expected open time
        shift_calc = control_dist / 15
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"small_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"small_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close

    def test_medium_close(self):
        # distances
        brevet_dist = 600
        control_dist = 450
        # expected open time
        shift_calc = 200 / 15 + 200 / 15 + 50 / 15
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"medium_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"medium_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close

    def test_big_close(self):
        # distances
        brevet_dist = 1000
        control_dist = 720
        # expected open time
        shift_calc = 200 / 15 + 200 / 15 + 200 / 15 + 120 / 11.428
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"big_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"big_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close

    def test_max_close(self):
        # distances
        brevet_dist = 1000
        control_dist = 1000
        # expected open time
        shift_calc = 200 / 15 + 200 / 15 + 200 / 15 + 400 / 11.428
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"max_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"max_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close

    def test_over_close(self):
        # distances
        brevet_dist = 600
        control_dist = 615
        # expected open time
        shift_calc = 200 / 15 + 200 / 15 + 200 / 15
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"over_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"over_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close

    def test_special_close(self):
        # distances
        brevet_dist = 200
        control_dist = 200
        # expected open time
        shift_calc = 13.5
        expected_close = self.race_time.shift(hours=shift_calc).isoformat()
        logging.debug(f"special_close: Expected close time is: {expected_close}")
        calculated_close = acp_times.close_time(control_dist, brevet_dist, self.race_time)
        logging.debug(f"special_close: Calculated close time is: {calculated_close}")
        assert calculated_close == expected_close
