"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

def make_arrow(date: str, time: str) -> "Arrow":
    year = int(date[:4])
    month = int(date[5:7])
    day = int(date[8:])
    hour = int(time[:2])
    minute = int(time[3:])
    return arrow.Arrow(year, month, day, hour, minute)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    app.logger.debug("request.args: {}".format(request.args))
    # get input brevet distance for race
    distance = request.args.get("brevet", type=str)
    # remove "km" off string ("200km" -> 200) and turn into int
    distance = int(distance[:-2])
    app.logger.debug("distance={}".format(distance))
    # get the input km for the race
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    race_date = request.args.get("date", type=str)
    app.logger.debug("race start date={}".format(race_date))
    race_time = request.args.get("time", type=str)
    app.logger.debug("race start time={}".format(race_time))
    start_race = make_arrow(race_date, race_time)
    app.logger.debug("iso-format race start={}".format(start_race.isoformat()))
    open_time = acp_times.open_time(km, distance, start_race)
    close_time = acp_times.close_time(km, distance, start_race)
    result = {"open": open_time, "close": close_time}
    app.logger.debug(f"times: {result}")
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
