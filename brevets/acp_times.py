"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from logging import debug

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# (min speed, max speed) up to and including a given number
BREVET_CONTROL_TIMES = {
    60: (20, 34),
    200: (15, 34),
    400: (15, 32),
    600: (15, 30),
    1000: (11.428, 28)
}


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # control points where speeds change
    control_times = BREVET_CONTROL_TIMES.keys()
    # cap control distance at brevet distance
    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km
    # time to cycle from race start to control open
    cycle_time = 0
    # distance cycled
    cycled = 0
    # add control times
    for dist in control_times:
        debug(f"open_time: testing {dist}km")
        # add control time for rate up to given distance
        if control_dist_km > dist:
            debug(f"open_time: over {dist}km")
            new_distance = dist - cycled
            cycle_time += new_distance / BREVET_CONTROL_TIMES[dist][1]
            cycled += new_distance
        # add control time for rate based on remaining distance
        else:
            remaining_dist = control_dist_km - cycled
            debug(f"open_time: {remaining_dist}km were left in the {dist}km category")
            cycle_time += remaining_dist / BREVET_CONTROL_TIMES[dist][1]
            break
    # shift race start time up given hours to make open time
    brevet_start_time = brevet_start_time.shift(hours=cycle_time)
    # return open time in standard iso format
    return brevet_start_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # control points where speeds change
    control_times = BREVET_CONTROL_TIMES.keys()
    # cap control distance at brevet distance
    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km
    # time to cycle from race start to control close (start w/ 1hr)
    cycle_time = 1
    # random case about 200km brevets
    if (control_dist_km == brevet_dist_km) and (brevet_dist_km == 200):
        cycle_time = 13.5
    # otherwise everything is normal
    else:
        # distance cycled
        cycled = 0
        # add control times
        for dist in control_times:
            debug(f"close_time: testing {dist}km")
            # add control time for rate up to given distance
            if control_dist_km > dist:
                debug(f"close_time: over {dist}km")
                new_distance = dist - cycled
                cycle_time += new_distance / BREVET_CONTROL_TIMES[dist][0]
                cycled += new_distance
            # add control time for rate based on remaining distance
            else:
                remaining_dist = control_dist_km - cycled
                debug(f"close_time: {remaining_dist}km were left in the {dist}km category")
                cycle_time += remaining_dist / BREVET_CONTROL_TIMES[dist][0]
                break
    # shift race start time up given hours to make close time
    brevet_start_time = brevet_start_time.shift(hours=cycle_time)
    # return close time in standard iso format
    return brevet_start_time.isoformat()

